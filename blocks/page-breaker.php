<div class="container-fluid">

    <?php $page_breaker_image = $container['page_breaker_background_image']; ?>

    <div class="parallax-window" data-parallax="scroll"  data-bleed="0" data-image-src="<?php echo $page_breaker_image['url']; ?>" data-natural-width="<?php echo $page_breaker_image['width']; ?>" data-natural-height="<?php echo $page_breaker_image['height']; ?>"></div>
</div>

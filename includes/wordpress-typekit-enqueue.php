<?php
/**
* Enqueue typekit fonts into WordPress using wp_enqueue_scripts.
*
* @author    Robert Neu
* @author    Eric Fernandez
* @copyright Copyright (c) 2014, Robert Neu
* @license   GPL-2.0+
* @link      http://typekit.com
*/

add_action( 'wp_enqueue_scripts', 'bd_enqueue_scripts' );
/**
 * Loads the main typekit Javascript. Replace your-id-here with the script name
 * provided in your Kit Editor.
 *
 * @todo Replace prefix with your theme or plugin prefix
 */
function bd_enqueue_scripts() {
	wp_enqueue_script( 'typekit', 'https://use.typekit.net/fgy5lby.js', array(), '1.0.0' );
}

add_action( 'wp_head', 'bd_typekit_inline' );
/**
 * Check to make sure the main script has been enqueued and then load the typekit
 * inline script.
 *
 *
 */
function bd_typekit_inline() {
	if ( wp_script_is( 'typekit', 'enqueued' ) ) {
		echo '<script type="text/javascript">try{Typekit.load();}catch(e){}</script>';
	}
}

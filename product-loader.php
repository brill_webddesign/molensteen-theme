<div class="ms-c-product__loader">

   <svg width="61px" height="120px" viewBox="0 0 61 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="ms-a-logo-molensteen--dark" fill="#60514E" fill-rule="nonzero">

            <path d="M1.55,92.98 C1.68,107.44 13.37,119.14 27.83,119.26 C27.7,104.8 16.01,93.1 1.55,92.98 L1.55,92.98 Z" id="Shape-path">
                <animate attributeType="CSS" attributeName="opacity"
from="0" to="1" dur=".7s" begin="0s" repeatCount="indefinite" />
            </path>


            <path d="M52.85,78.48 C40.27,80.71 30.72,91.7 30.72,104.91 L31.58,119.25 C32.92,119.24 33.37,119.12 34.67,118.92 C47.33,116.92 57.02,105.96 57.02,92.73 C57.016432,87.6796635 55.569623,82.735533 52.85,78.48 L52.85,78.48 Z" id="Shape-path">
                <animate attributeType="CSS" attributeName="opacity"
from="0" to="1" dur=".7s" begin="0.1s" repeatCount="indefinite" />
            </path>

            <path d="M4.59,49.46 C17.17,51.7 26.72,62.68 26.72,75.9 L25.86,90.24 C24.52,90.23 24.06,90.11 22.77,89.91 C10.1,87.91 0.42,76.95 0.42,63.72 C0.420714,58.6661257 1.867675,53.7180036 4.59,49.46 L4.59,49.46 Z" id="Shape-path">
                <animate attributeType="CSS" attributeName="opacity"
from="0" to="1" dur=".7s" begin=".2s" repeatCount="indefinite" />
            </path>

            <path d="M58.12,50.63 C57.99,65.09 46.3,76.79 31.84,76.91 C31.96,62.45 43.66,50.75 58.12,50.63 L58.12,50.63 Z" id="Shape-path">
                <animate attributeType="CSS" attributeName="opacity"
from="0" to="1" dur=".7s" begin=".3s" repeatCount="indefinite" />
            </path>

            <path d="M4.09,16.3 C0.6,32.34 10.61,48.21 26.59,51.98 C30.08,35.94 20.06,20.06 4.09,16.3 L4.09,16.3 Z" id="Shape-path">
                <animate attributeType="CSS" attributeName="opacity"
from="0" to="1" dur=".7s" begin=".4s" repeatCount="indefinite" />
            </path>

            <path d="M56.48,9.48 C43.9,11.71 34.35,22.7 34.35,35.91 L35.21,50.25 C36.55,50.24 37,50.13 38.29,49.92 C50.96,47.92 60.64,36.96 60.64,23.73 C60.64,18.49 59.11,13.59 56.48,9.48 L56.48,9.48 Z" id="Shape-path">
                <animate attributeType="CSS" attributeName="opacity"
from="0" to="1" dur=".7s" begin=".5s" repeatCount="indefinite" />
            </path>

            <path d="M22.45,0 C29.41,4.17 31.75,13.15 27.71,20.19 C20.75,16.02 18.41,7.04 22.45,0 L22.45,0 Z" id="Shape-path">
                <animate attributeType="CSS" attributeName="opacity"
from="0" to="1" dur=".7s" begin=".6s" repeatCount="indefinite" />
            </path>

        </g>
    </g>
   </svg>
</div>

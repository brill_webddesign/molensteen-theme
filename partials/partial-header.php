<?php $page_header_image = get_field( 'page_header_image' ); ?>

<?php if ( $page_header_image ) { ?>

<div class="ms-c-header-slider">
    <div class="owl-carousel" id="header_carousel">
        <div class="item  d-flex  align-items-end  ms-c-header-slider__item  embed-responsive  embed-responsive-18by9" style="background-image: url('<?php echo $page_header_image['url']; ?>')">
            <div class="ms-c-header-slider__item--content">
                <h2  class="ms-u-header-slider__title" ><?php the_field( 'page_header_title' ); ?></span></h2>
            </div>
        </div>
    </div>
</div>

<?php } ?>

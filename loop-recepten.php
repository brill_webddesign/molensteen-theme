<a href="<?php the_permalink(); ?>">
    <div class="ms-c-recipe">
        <div class="ms-c-recipe__image"><?php the_post_thumbnail('recipe-loop'); ?></div>
        <div class="ms-c-recipe__content">
            <h4 class="ms-u-recipe-title"><?php the_field('recipe_short_title'); ?></h4>
            <p class="ms-u-p--description"><?php the_excerpt(); ?></p>
        </div>
    </div>
</a>

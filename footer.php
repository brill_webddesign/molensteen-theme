
</main>

    <footer>
        <div class="container">
            <!-- <div class="row">
                <div class="col-md-12  text-center  mx-auto  ms-c-newsletter">
                    <div class="ms-c-newsletter__content">
                        <form class="newsletter" action="index.html" method="post">
                            <input type="text" name="" value="" placeholder="Vul hier je e-mailadres in"><input type="submit" name="" value="inschrijven nieuwsbrief" class="ms-u-bg--green-yellow"><input type="submit" name="" value="uitschrijven" class="ms-u-bg--terracotta">
                        </form>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-md-4 col-lg-2 offset-lg-1  ms-c-footer__menu">
                    <h5>Snel naar</h5>
                    <?php $args = array(
                        'menu' => 'Footer Menu',
                        'container' => 'ul',
                        'menu_class' => 'ms-c-footer-menu'
                    );

                    wp_nav_menu( $args );

                    ?>

                </div>
                <div class="col-md-4 col-lg-3 offset-md-3 offset-lg-2  ms-c-footer__contact">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/ms-a-stuur-een-mailtje.png" class="ms-c-stuur-een-mailtje" />
                    <h5>Contact</h5>
                    <?php if( is_active_sidebar( 'footer-contact' ) ) : ?>

                            <?php dynamic_sidebar( 'footer-contact' ); ?>

                    <?php endif; ?>
                </div>
                <div class="col-12 col-lg-3 offset-lg-1  ms-c-footer__follow">
                    <h5>Volg ons</h5>
                    <?php if( is_active_sidebar( 'footer-social' ) ) : ?>

                            <?php dynamic_sidebar( 'footer-social' ); ?>

                    <?php endif; ?>

                </div>
            </div>
        </div>
    </footer>
<?php wp_footer() ?>
</body>
</html>

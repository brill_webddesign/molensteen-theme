<a href="<?php the_permalink(); ?>">
    <div class="ms-c-recipe">

        <?php

        $image = get_field('custom_thumbnail');
        $size = 'recipe-loop'; // (thumbnail, medium, large, full or custom size)

        if( $image != '') : ?>

            <div class="ms-c-recipe__image"><?php echo wp_get_attachment_image( $image, $size ); ?></div>

        <?php else : ?>

            <div class="ms-c-recipe__image"><?php the_post_thumbnail('recipe-loop'); ?></div>

        <?php endif; ?>

        <div class="ms-c-recipe__content">
            <h4 class="ms-u-recipe-title"><?php the_field('blog_short_title'); ?></h4>
            <?php the_excerpt(); ?>
        </div>
    </div>
</a>

<?php get_header() ?>

<main>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="container  ms-c-first-container">

        <div class="row  align-items-center">
            <div class="col-md-6 text-center my-auto">
                <?php the_post_thumbnail('large'); ?>
            </div>
            <div class="col-md-6">
                <div class="ms-c-product-content">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>

                </div>
            </div>
        </div>

    </div>

    <?php bd_page_blocks(); ?>


<?php endwhile; ?>

<?php get_footer() ?>
